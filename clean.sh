#!/usr/bin/env bash

function press (){
  read -n 1 -s -r -p "Press any key to continue"
}

command1="docker stop hello-world"
echo $command1
press
$command1

command2="docker rmi -f hello-world:latest"
echo $command2
press
$command2
